# The Andromeda Framework

### The Main Goal
To create a framework for people to create, display and share dynamic stories  

### What are you talking about?
Clone the repositry and check the demo with these steps:
1. Run `example.py` using Python 3.6 or higher
2. Browse to `localhost:5000` using a web browser

### But it's just a simple game, what is the big deal?
The big deal is that the website is not programmed to display this sepcific book/game.  
It is built to display **every** book.
Please read the [wiki](https://gitlab.com/Andromedous/Andromeda/wikis/home) for more information