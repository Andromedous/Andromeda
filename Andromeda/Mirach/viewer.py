from flask import Flask, render_template, send_from_directory
from ..Alpheratz import Parser, Story

import os
import sys

class Viewer:
    def __init__(self, debug=False):
        self.app = Flask(__name__)
        self.app.add_url_rule('/', 'index', self.index)
        self.app.add_url_rule('/favicon.ico', 'favicon', self.favicon)
        self.app.add_url_rule('/<block_name>', 'block', self.show_block)
        self.debug_mode = debug

    def debug(self, msg):
        if self.debug_mode:
            print(msg)

    def run(self, story_name):
        self.story_name = story_name
        self.app.static_folder = os.path.join(os.getcwd(), self.story_name)
        self.story = Parser(self.story_name, debug=self.debug_mode).parse()
        self.app.run(debug=self.debug_mode, threaded=True, host='0.0.0.0')

    def favicon(self):
        return send_from_directory(self.app.static_folder, 'icon.png')

    def index(self):
        self.debug(f'Asking for index, redirecting to {self.story.start}')
        return self.show_block(self.story.start)

    def get_info(self, info_name, info_type):
        if info_type == 'text':
            f = open(os.path.join(
                self.story_name, 'information', info_name + '.txt'))
            data = f.read().strip('\n')
            f.close()
            return data
        elif info_type == 'image':
            return os.path.join('information', info_name + '.png')
        else:
            raise ValueError(f'{info_type} is not a valid type')


    def format_operation(self, operation):
        operation_signs = {'assign': '=', 'add': '+=', 'subtract': '-='}
        return (operation.var_name,
                operation_signs[operation.operation],
                operation.value)

    def format_info(self, info_name):
        info = self.story.infos[info_name]
        return (info.info_type, self.get_info(info.info_name,
            info.info_type))

    def format_option(self, option_name):
        option = self.story.options[option_name]
        test_signs = { 'equals': '==',
                'lower_than': '<', 'greater_than': '>'}
        tests = [(t.var_name, test_signs[t.test_type], t.value)
                for t in self.story.conditions[option.condition].tests] \
                if option.condition else []
        return (self.format_info(option.option_info),
                option.block_name, tests)

    def show_block(self, block_name):
        self.debug(f'Requesting {block_name}')
        block = self.story.blocks[block_name]
        start = True if block_name == self.story.start else False

        operations_todo = [self.format_operation(o)
                for o in block.operations]
        infos_toview = [self.format_info(i) for i in block.block_infos]
        options_toview = [self.format_option(o) for o in
                self.story.choices[block.block_choice].choice_options]

        self.debug(f'Sending to template:\nstory_name={self.story_name}\n' \
                f'options={options_toview}\ninfos={infos_toview}\n' \
                f'start={start}\n' \
                f'variables={self.story.variables.values()}\n' \
                f'operations={operations_todo}')
        return render_template('block.html', story_name=self.story_name,
                options=options_toview, infos=infos_toview, start=start,
                variables=self.story.variables.values(),
                operations=operations_todo)
