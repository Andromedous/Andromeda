import os
from .objects import Story, Information, Option, Variable, Operation, Test, Block, Choice, Condition

class Parser():
    def __init__(self, story_name, debug=False):
        self.debug_mode = debug
        self.story_name = story_name
        try:
            f = open(os.path.join(story_name, 'instructions'))
        except:
            raise ValueError(
                    f'Could not find instructions for story {story_name}')
        self.code = list(map(lambda x: x.strip('\n'), f.readlines()))
        f.close()
        self.story = Story()

    def split(self, s,sep,n):
        splits = s.split(sep)
        if len(splits) != n:
            raise SyntaxError('{0}\nBad argument count'.format(s))
        elif not all(splits):
            raise SyntaxError('{0}\nEmpty arguments'.format(s))
        else:
            return splits

    def debug(self, text):
        if self.debug_mode:
            print(text)

    def parse(self):
        TOKEN_SIZE = 3
        SINGLES = ['##<', '$$<', '!!<', '&&<']
        BLOCK_CLOSERS = {
                '@@<': '@@>',
                '%%<': '%%>',
                '**<': '**>' }
        FUNCTIONS = {
                '##<': self.parse_info,
                '$$<': self.parse_option,
                '@@<': self.parse_block,
                '%%<': self.parse_choice,
                '**<': self.parse_condition,
                '&&<': self.parse_variable,
                '!!<': self.parse_start }
        current_block = None
        progress = []
        self.debug('Parse starting')
        for line_number, line in enumerate(self.code):
            self.debug(f'@{line_number} is {line}')
            if not line or line == '\n' or line == '\t':
                self.debug(f'@{line_number} Empty line')
                continue
            start = line[:TOKEN_SIZE]
            self.debug(f'@{line_number} Start token is {start}')
            if current_block:
                progress.append(line)
                self.debug(f'@{line_number} Added line to' \
                        f' {current_block} block progress')
                if start == BLOCK_CLOSERS[current_block]:
                    self.debug(f'@{line_number} Found block end')
                    FUNCTIONS[current_block](progress)
                    progress = []
                    current_block = None
                    continue
                else:
                    continue
            else:
                if start in SINGLES:
                    self.debug(f'@{line_number} Recognized line start ' \
                    f'as single')
                    FUNCTIONS[start](line)
                    continue
                elif start in BLOCK_CLOSERS.keys():
                    self.debug(f'@{line_number} Recognized line start ' \
                    f'as a block start')
                    current_block = start
                    progress.append(line)
                    continue
                else:
                    raise ValueError(f'Unknown line start - {start}')
        
        if current_block is not None:
            raise ValueError(f'Parse ended mid block - {current_block}')
        if self.debug_mode:
            self.story.dump()
        self.check()
        return self.story
    

    def parse_info(self, line):
        types = {'text': '.txt', 'image': '.png'}
        info_name, info_type = self.split(line[3:], '>', 2)
        if info_type not in types.keys():
            raise SyntaxError(
                    f'{info_type} is not a known info type\n{line}')
        elif info_name in self.story.infos.keys():
            raise NameError(
                    f'{info_name} is already an information name\n{line}')
        else:
            path = os.path.join(self.story_name,
                    'information', info_name + types[info_type])
            if not os.path.isfile(path):
                raise ValueError(f'{info_name} is not at {path}')
            else:
                self.story.infos[info_name] =  Information(
                        info_name, info_type)

    def parse_option(self, line):
        try:
            option_name, info_name, block_name, cond_name = self.split(
                    line[3:], '>', 4)
        except SyntaxError as e:
            if 'Empty arguments' in str(e):
                option_name, info_name, block_name = self.split(
                        line[3:-1], '>', 3)
                cond_name = None
            else:
                raise e
        if option_name in self.story.options.keys():
            raise NameError(
                    f'{option_name} is already an option name\n{line}')
        else:
            self.story.options[option_name] = Option(
                    option_name, info_name, block_name, cond_name)
        
    def parse_variable(self, line):
        types = ['visible', 'hidden']
        variable_name, default_value, variable_type = self.split(
                line[3:], '>', 3)
        if variable_type not in types:
            raise ValueError(
                    f'{variable_type} is not a known variable type\n{line}')
        elif variable_name in self.story.variables.keys():
            raise NameError(
                    f'{variable_name} is already a variable name\n{line}')
        else:
            self.story.variables[variable_name] =  Variable(
                    variable_name, default_value, variable_type)

    def parse_choice(self, lines):
        if len(lines) < 3:
            raise SyntaxError('Choice chunk empty')
        choice_name = lines[0][3:]
        if not choice_name:
            raise NameError(f'Choice name empty\n{line}')
        elif choice_name in self.story.choices.keys():
            raise NameError(
                    f'{choice_name} is already a choice name\n{line}')
        options = []
        for line in lines[1:-1]:
            if line.startswith('$>'):
                option_name = line[2:]
                if option_name:
                    options.append(option_name)
                else:
                    raise SyntaxError(
                        f'Empty option in Choice chunk\n{lines[0]}')
            else:
                raise SyntaxError(
                        f'Unkown start-tag in Choice chunk\n{line}')
        self.story.choices[choice_name] = Choice(choice_name, options)

    def parse_block(self, lines):
        if len(lines) <= 2:
            raise SyntaxError('Block chunk empty')
        block_name = lines[0][3:]
        if not block_name:
            raise NameError(f'Block name empty\n{line[0]}')
        elif block_name in self.story.blocks.keys():
            raise NameError(f'{block_name} is already a block name\n{line}')
        choice_name = lines[-1][3:]
        if not choice_name:
            raise NameError(f'Choice name empty\n{line[-1]}')
        infos = []
        operations = []
        for line in lines[1:-1]:
            if line.startswith('#>'):
                info_name = line[2:]
                if info_name:
                    infos.append(info_name)
                else:
                    raise SyntaxError(
                            f'Empty info in Block chunk\n{lines[0]}')
            elif line.startswith('&>'):
                operations_allowed = ['assign', 'add', 'subtract']
                var_name, operation, value = self.split(line[2:], '>', 3)
                if operation not in operations_allowed:
                    raise ValueError(
                            f'{operation} is not a known operation\n{line}')
                operations.append(Operation(var_name, operation, value))
            else:
                raise SyntaxError(
                        f'Unkown start-tag in Block chunk\n{line}')
        self.story.blocks[block_name] =  Block(
                block_name, infos, choice_name, operations)

    def parse_condition(self, lines):
        test_types = ['lower_than', 'equals', 'greater_than']
        if len(lines) < 3:
            raise SyntaxError('Condition chunk empty')
        cond_name = lines[0][3:]
        if not cond_name:
            raise NameError(f'Condition name empty\n{line[0]}')
        elif cond_name in self.story.conditions.keys():
            raise NameError(
                    f'{cond_name} is already a Condition name\n{line}')
        tests = []
        for line in lines[1:-1]:
            if line.startswith('*>'):
                var_name, test_type, value = self.split(line[2:], '>', 3)
                if test_type not in test_types:
                    raise ValueError(
                            f'{test_type} is not a known test type\n{line}')
                tests.append(Test(var_name, test_type, value))
            else:
                raise SyntaxError(
                        f'Unkown start-tag in Condition chunk\n{line}')
        self.story.conditions[cond_name] = Condition(cond_name, tests)

    def parse_start(self, line):
        try:
            self.story.start = line[3:]
        except:
            raise SyntaxError(f'Invalid start - {line}')

    def check(self):
        if self.story.start == None:
            raise ValueError('Story starting block not set')
        elif self.story.start not in self.story.blocks.keys():
            raise ValueError('Starting block {0} does not exist'.format(
                self.story.start))

        for option in self.story.options.values():
            if option.option_info not in self.story.infos.keys():
                raise ValueError(
                        f'{option.option_info} is not an info name\n' \
                        f'referenced by {option.option_name}')
            elif option.block_name not in self.story.blocks.keys():
                raise ValueError(
                        f'{option.block_name} is not a block name\n' \
                        f'referenced by {option.option_name}')

        for cond in self.story.conditions.values():
            for test in cond.tests:
                if test.var_name not in self.story.variables:
                    raise ValueError(
                            f'{test.var_name} is not a variable name\n' \
                            f'referenced by {cond.cond_name}')

        for choice in self.story.choices.values():
            for option in choice.choice_options:
                if option not in self.story.options.keys():
                    raise ValueError(
                            f'{option} is not an option name\n' \
                            f'referenced by {choice.choice_name}')

        for block in self.story.blocks.values():
            for info in block.block_infos:
                if info not in self.story.infos.keys():
                    raise ValueError(f'{info} is not an info name\n' \
                    f'referenced by {block.block_name}')
            if block.block_choice not in self.story.choices.keys():
                raise ValueError(
                    f'{block.block_choice} is not a choice name\n' \
                    f'referenced by {block.block_name}')
