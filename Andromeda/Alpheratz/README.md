# The Alpheratz Module
The Alpheratz module actually consists of two different things:  
* **The Protocol**, Which contains the instructions on how to build a story
* **The Parser**, Which parses stories in the Alpheratz into a python class

## The Protocol
The protocol is a set of symbols and rules that, when put together, represent the story  
Each story consists of a directory that contains:
1. The instruction file
2. The data

### The Instruction File
This is the most important part of the protocol, which determines how the story will play out  
Here are the basic pieces that make up an instruction file:

#### Information
The basic unit of a story. This is a piece of text / an image.  
Format:  `##<1>2`
1. The name of the information (as an id, to be used later in the file)
2. The type of the information (text/image)

Example: `##<mike_final_words>text`  
note: The name of the information will also be the filename the parser will look for (examples in `The data` section)  

#### Options
An option is a single thing the reader can read in a choice (explained in `Choice`)  
A number of options (one and more) make up one multiple-option choice
Format: `$$<1>2>3>`
1. The name of the option
2. The name of the information to display as the option
3. The name of the block to go to if option is chosen (explained in `Blocks`)

Example:
```
##<cave>image
$$<enter_cave>cave>inside_cave>
```

#### Choice
A collection of options presented to the reader at once
Format:
```
%%<1
$>2
$>2
...
$>2
%%>
```
1. The name of the choice
2. A list of option names to display

Example:
```
##<stay_home>text
##<go_outside>text

$$<stay_home>stay_home>home
$$<go_outside>go_outside>yard

%%<where_to_go
$>stay_home
$>go_outside
%%>
```

#### Block
What is actually displayed to the reader. Each block can be thought of as a "page" or a "scene"
Format:
```
@@<1
#>2
#>2
...
#>2
@@>3
```
1. The block name
2. A list of information's names to display (in order)
3. The choice to display at the end of the block

Example:
```
##<home>image
##<radio_talk>text
##<where_should_go>text

##<stay_home>text
##<go_outside>text

$$<stay_home>stay_home>home
$$<go_outside>go_outside>yard

%%<where_to_go
$>stay_home
$>go_outside
%%>

##<home>image
##<radio_talk>text

@@<home
#>home
#>radio_talk
#>where_should_go
@@>where_to_go

!!<home
```
**Note:** The `!!<` represents the block that is displayed at the start of the story.  
This example is a story with a block called "home" that displays a home image, some radio talk, and the question where to go
Then, the reader in this block will be presented with a choice - to stay home or to go outside.
If the reader chooses "stay_home", he is redirected to the same home block (nothing happens)
If the reader chooses "go_outside", he is redirected to the "yard" block (not written in this example)  
  
**Note:** A block with no information can be created, but a choice is mandatory,
as this is the system to travel between blocks (and progress the story)

### A More Modular Instruction File
To make stories more modular, variables and conditions are added into the protocol
#### Variables
Variables act like variables in programming, they have a name and a mutable value.
##### Declaration
Format:  `&&<1>2>3`
1. The name of the variable
2. The default value of the variable
3. (optional) Whether it is visible or hidden to the reader

Examples:
``` 
&&<is_cave_explored>false>hidden
&&<money>0>
```
##### Operations
Variable operations are made inside blocks. The operation will occuer each time the block is displayed
Format:
```
@@<block_name
#>info1
#>info2
&>1>2>3
&>1>2>3
..
@@>
```
1. The name of the variable to do the operation
2. The operation (assign, add, subtract)
3. The value of the operation

Examples:

```
&&<is_cave_explored>false>hidden

@@<cave
#>cave_img
&>is_cave_explored>assign>true
#>cave_txt
@@>where_to_now

@@<sword_bought
&>money>subtract>100
#>sword
#>congrats_txt
@@>back_to_town
```

#### Conditions
Conditions are an addition to options. The option of said choice will only be displayed if the condition is true
Format: `$$<option_name>option_info>block_name>1`
1. The name of the condition

Each condition is a list of tests, each test in a line
Condition format:
```
**<1
*>2>3>4
*>2>3>4
...
*>2>3>4
**>
```
1. The name of the condition
2. The variable to check within a given test
3. What check to perform (lower_than, equals, greater_than)
4. The value to test against

Example:
```
**<can_has_dragon_sword
*>is_dragonborn>equals>true
*>money>greater_than>599
*>

$>buy_sword>yes>sword_bought>can_has_dragon_sword
$<dont_buy>no>town

%%<should_buy_sword
$>buy_sword
$>dont_buy
%>

@@<ask_to_buy_sword
#>sword_img
#>merchant_desc
#>should_buy_question
@>should_buy_sword
```