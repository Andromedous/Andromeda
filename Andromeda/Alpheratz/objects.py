class Information:
    def __init__(self, info_name, info_type):
        self.info_name = info_name
        self.info_type = info_type
    def __str__(self):
        return self.info_name


class Option:
    def __init__(self, option_name, option_info, block_name, condition=None):
        self.option_name = option_name
        self.option_info = option_info
        self.block_name = block_name
        self.condition = condition
    def __str__(self):
        return self.option_name

class Variable:
    def __init__(self, var_name, default_value, var_type):
        self.var_name = var_name
        self.default_value = default_value
        self.var_type = var_type
    def __str__(self):
        return self.var_name

class Test:
    def __init__(self, var_name, test_type, value):
        self.var_name = var_name
        self.test_type = test_type
        self.value = value

class Condition:
    def __init__(self, cond_name, tests):
        self.cond_name = cond_name
        self.tests = tests
    def __str__(self):
        return self.cond_name

class Choice:
    def __init__(self, choice_name, choice_options):
        self.choice_name = choice_name
        self.choice_options = choice_options
    def __str__(self):
        return self.choice_name

class Operation:
    def __init__(self, var_name, operation, value):
        self.var_name = var_name
        self.operation = operation
        self.value = value

class Block:
    def __init__(self, block_name, block_infos, block_choice, operations):
        self.block_name = block_name
        self.block_infos = block_infos
        self.block_choice = block_choice
        self.operations = operations
    def __str__(self):
        return self.block_name

class Story:
    def __init__(self, infos = {}, options = {}, choices = {}, blocks = {}, conditions={}, variables={}, start = None):
        self.infos = infos
        self.options = options
        self.choices = choices
        self.blocks = blocks
        self.conditions = conditions
        self.variables = variables
        self.start = start

    def dump(self):
        for key, val in self.__dict__.items():
            if key == 'start':
                print(f'Start: {val}', end='\n\n')
            else:
                for i in val.values():
                    for ik, iv in i.__dict__.items():
                        print(f'{ik}: {iv}')
                    print('')
                print('')

